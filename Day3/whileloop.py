#While loop in Phython
#Print i as long as i is less than 6
i=1
while i<6:
    print('welcome')
    i+=1 #Equalsi=i+1

else:
    print("loop success")
#Program to add natural numbers up to 5
n=5
sum=0
i=1
while i<=n:
    sum=sum+i
    i+=1
else:
    print("Looping completed successfully")
    print("The sum is",sum)

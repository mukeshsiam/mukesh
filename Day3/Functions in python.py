'''Write a function called calculate_area that takes base and height as an input and returns
and area of a triangle.
Equation of an area of a triangle is,
area = (1/2)*base*height'''
# The Program1
def calculate_area(base=0,height=0):
    area=(1/2)*base*height
    return area
base=float(input("Enter the base of Triangle\n"))
height=float(input("Enter the height of Triangle\n"))
area=calculate_area(base,height)
print("The Area of Triangle:",area)
'''Modify above function to take third parameter shape type. 
It can be either "triangle" or "rectangle". Based on shape type it will calculate area. 
Equation of rectangle's area is,
rectangle area=length*width
If no shape is supplied then it should take triangle as a default shape'''
# # The Program2
def calculate_area(dim1=0,dim2=0,shape='triangle'): #setting default parameters
    if shape=="triangle":
        area=(1/2)*dim1*dim2
    elif shape=="rectangle":
        area=dim1*dim2
    else:
        print("Error shape is neither Rectangle nor triangle")
        area="none"
    return area
# Area of Triangle
base=10
height=2
shape="triangle"
areaT=calculate_area(base,height,shape)
print("Area of Triangle",areaT)
# # Area of rectangle
length=20
width=10
areaR=calculate_area(length,width,"rectangle")
print("Area of rectangle",areaR)
# Area of triangle with no shape supplied
areaT=calculate_area(base,height)
print("Area of Triangle with  no shape supplied:",areaT)
# Area of square
side=5
shape="square"
areaS=calculate_area(side,side,shape)
print("Area of square",areaS)
'''Write a function called print_pattern that takes integer number as an argument and prints following pattern if input number is 3,
*
**
***
'''
def print_pattern(num):
    for i in range(1,num):
        s=''
        for j in range(i):
            s=s+'*'
    return s
print(print_pattern(4))
def print_pattern(n=5):

    for i in range(n):
        s = ''
        for j in range(i+1):
            s = s + '*'
        print(s)
num=int(input("Enter any integer"))
print(f"Print pattern with input={num}")
print_pattern(num)



#Exceptions are errors occur while executing the program
#Handling exceptions are how to overcome the error or crash(error)
num1=int(input("Enter a number1\n"))
num2=int(input("Enter a number2\n")) #If the user inputs 0 for num2 it leads to "ZeroDivisionError: division by zero".
#To overcome"ZeroDivisionError: division by zero".We should do exception handling
try:
    z=num1/num2

#To find type of exception
except Exception as e:
    print("Exception type",type(e).__name__)
    z="non"
print("answer:",z)
'''Exercise
You're going to write an interactive calculator! User input is assumed to be a formula that consist of a number,
 an operator (at least + and -), and another number, separated by white space (e.g. 1 + 1). 
 Split user input using str.split(), and check whether the resulting list is valid:

If the input does not consist of 3 elements, raise a FormulaError, which is a custom Exception.
Try to convert the first and third input to a float (like so: float_value = float(str_value)). 
Catch any ValueError that occurs, and instead raise a FormulaError
If the second input is not '+' or '-', again raise a FormulaError
If the input is valid, perform the calculation and print out the result. 
The user is then prompted to provide new input, and so on, until the user enters quit.

An interaction could look like this:

>>> 1 + 1
2.0
>>> 3.2 - 1.5
1.7000000000000002
>>> quit'''
class FormulaError(Exception): pass


def parse_input(user_input):

  input_list = user_input.split()
  if len(input_list) != 3:
    raise FormulaError('Input does not consist of three elements')
  n1, op, n2 = input_list
  try:
    n1 = float(n1)
    n2 = float(n2)
  except ValueError:
    raise FormulaError('The first and third input value must be numbers')
  return n1, op, n2


def calculate(n1, op, n2):

  if op == '+':
    return n1 + n2
  if op == '-':
    return n1 - n2
  if op == '*':
    return n1 * n2
  if op == '/':
    return n1 / n2
  raise FormulaError('{0} is not a valid operator'.format(op))


while True:
  user_input = input('>>> ')
  if user_input == 'quit':
    break
  n1, op, n2 = parse_input(user_input)
  result = calculate(n1, op, n2)
  print(result)
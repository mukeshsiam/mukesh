def evens(start,stop):
    for even in range(start,stop+1,2):
        yield(even)
def main():
    even_values=[even for even in evens(20,40)] #List compherension
    even2=tuple(evens(20,30))
    print(even_values)
    print(even2)
m=main()
g=evens(4,10)
print(next(g))
print(next(g))
print(next(g))
print(next(g))
#print(next(g))#comes error message because iteration finished
print(list(g)) #Empty list because no value to be iterated
g1=evens(2,8)
print(list(g1))#the output can be in the form of list also
#Another method using for loop to yield generators
g2=evens(8,20)
for x in g2:
    print(x)

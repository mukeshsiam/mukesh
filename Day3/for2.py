# Using for loop figure out how many times you got heads from the result
result = ["heads","tails","tails","heads","tails","heads","heads","tails","tails","tails"]
count=0
for i in result:
    if i=="heads":
        count=count+1
print("No. of heads: ",count)
# Print square of all numbers between 1 to 10 except even numbers
for i in range(1,11):           #The range function end index is end value+1
   if i%2==0:
       continue
   else:
        print(i*i)
# a program that asks you to enter an expense amount and program should tell you in which month that expense occurred. If expense is not found then it should print that as well.
expense_list = [2340, 2500, 2100, 3100, 2980]
month_list=["jan","feb","mar","apr","may"]
exp=input("Enter the expense amount\n")
exp=int(exp)
i=0
for i in range(len(expense_list)):
  if exp==expense_list[i]:
      print("The expense of ",str(exp),"is made in ",month_list[i])
# 4. Lets say you are running a 5 km race. Write a program that,
#    1. Upon completing each 1 km asks you "are you tired?"
#    2. If you reply "yes" then it should break and print "you didn't finish the race"
#    3. If you reply "no" then it should continue and ask "are you tired" on every km
#    4. If you finish all 5 km then it should print congratulations message
for i in range(5):
    print(f"you ran {i+1} miles")
    tired=input("Are you tired?")


    if tired=="yes":
        break


    elif tired=="no":
        continue

    else:
        print("please type 'yes' or 'no'")
        # print("you didn't finish the race")

    
    if i==4:
        print("You successfully completed the race")
# 5. Write a program that prints following shape
# *
# **
# ***
# ****
# *****
num=input("Enter a number for n pattern\n")
num=int(num)
for i in range(1,num):
    s = ''
    for j in range(i):
        s += '*'
    print(s)

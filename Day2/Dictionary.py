#Dictionary in Phython
data={1:'Muki',2:'Hari',3:'Jaga'}
print(data)

print(data[1])
print(data[2])
print(data[3])
print(data.get(3))#This method can also be used
print(data.get(4,'not found'))   #If the given value is not found display 'not found'
#How to create Dictionaries using list
actors=['rajni','kamal','ajith']
movie=['robo','vikram','red']
cini=dict(zip(actors,movie))   #'dict' is used to convert zipped lists to dictionary
print(cini)
#Dictionary and its functions
cini['vijay']='mersal'#added new key:value in dict
print(cini)
del(cini['kamal'])#'del' is used to delete an element by key
print(cini)
cini['ajith']='veeram' #used to change value for the key
print(cini)
#Dictionary and its operations
thisdict={'brand':'Ford',
          'model':'mustang',
            'year':1964}
print('1.',thisdict)
x=thisdict['brand']
print(x)
print(len(thisdict))#used to find the length of dictionary
#How to remove Data in dictionary
thisdict.pop('brand')  #using pop method
print(thisdict)
thisdict.popitem()   #Using this methhod the final key:value is removed
print(thisdict)
#Copying a Dictionary
mydict=thisdict.copy()      #Using copy() function
print(mydict)
mydict1=dict(thisdict)      #Using dict function
print(mydict1)
#list and its operations

num=[12.5,34,56,24,34]
#append
num.append(94)   #append-adding the element in the last memory space
print(num)
num.sort()  #sort the elements in a list
print(num)
num.pop(2)    #pop means removing the element in the index value mentioned
print(num)
fruits=['javck','lemon','goa','p.apple']
print(fruits+num)#add 2 list
print(num)
print(num.count(24))  #count-used to count the elements in a list
num.extend(fruits) #extend used to add another list at the end
print(num)
print(num.index(34))#index-gives the lowest index value of the given element
print(num.insert(2,38))#inset-used to insert the given elemnt in given index
print(num.remove(24))#remove-used to remove the given value
num.reverse()#Reverse-used to reverse the entire list
print(num)
fruits=num.copy() #copy used to copy one list to others
print(fruits) #now nums is copied to fruits

num.clear()    #clear the elements in a list with a square brkt
print(num)

del num
# print(num) #deletes the entire list and shows error



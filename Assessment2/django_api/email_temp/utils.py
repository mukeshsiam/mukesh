from django.core.mail import EmailMessage
from rest_framework.fields import set_value
from django_api.settings import EMAIL_HOST_USER



class Util:
    @staticmethod
    def send_email(data):
        
        email=EmailMessage(subject=data['email_subject'],body=
                           data['email_body'],to=[data['to_email']])
        file=open("README.MD",'r')
        email.attach("README.md",file.read())
        
        email.send()
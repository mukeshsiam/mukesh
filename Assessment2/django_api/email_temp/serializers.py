from os import name
from rest_framework import serializers
from .models import Submit
from rest_framework import serializers



class SubmitSerializer(serializers.ModelSerializer):
    Name=serializers.CharField()
    mail=serializers.EmailField(max_length=255)
    age=serializers.IntegerField()
    phone=serializers.IntegerField()
    class Meta:
        model = Submit
        fields = ('__all__')
        
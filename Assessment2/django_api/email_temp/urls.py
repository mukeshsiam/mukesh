from django.urls import path,include
from .views import RegisterView
from rest_framework.routers import DefaultRouter
router=DefaultRouter()

urlpatterns = [
    path('',RegisterView.as_view(),name="Register")
]
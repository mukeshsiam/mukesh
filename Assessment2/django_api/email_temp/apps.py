from django.apps import AppConfig


class EmailTempConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'email_temp'

from django.core.mail import EmailMessage, message
from django.template import Context
from rest_framework.fields import set_value
from django_api.settings import EMAIL_HOST_USER
from django.template.loader import render_to_string ,get_template

class Util:
    @staticmethod
    def send_email(data):
        # import html message.html file
        # import sys
        # sys.path.insert(1, r'C:\Users\mukiv\DJANGO\Assessment2\django_api')

        ctx= {'name': data['name']}
        message=get_template('email_temp/mail_template.html').render(ctx)
        

        
        email=EmailMessage(subject=data['email_subject'],body=
                           message,to=[data['to_email']])
        email.content_subtype='html'
        file=open("README.MD",'r')
        email.attach("README.md",file.read())
        
        email.send()



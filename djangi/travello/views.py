from django.shortcuts import render
import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, r'C:\Users\mukiv\PycharmProjects\Djangopro\djangi')

from travello.models import Destination



# Create your views here.
def index(request):
    dest1=Destination()
    dest1.name='Chennai'
    dest1.desc='city with good hearted people'
    dest1.price=720
    dest1.img='destination_1.jpg'

    dest2=Destination()
    dest2.name='Tanjore'
    dest2.desc='Rice bowl of india'
    dest2.price=700
    dest2.img='destination_2.jpg'

    dest3=Destination()
    dest3.name='Coimbatore'
    dest3.desc='Next chennai'
    dest3.price=750
    dest3.img='destination_3.jpg'

    dests=[ dest1, dest2, dest3 ]
    return render(request,"index.html", {"dests": dests})


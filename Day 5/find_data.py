import pymongo
url="mongodb://localhost:27017/"
myclient=pymongo.MongoClient(url)
print(myclient.list_database_names())
mydb=myclient["mukidb1"]
mycollection=mydb["customers"]
# myd2={"_id":"1","name":"ram","address":"goa"}
# x=mycollection.insert_one(myd2)
#TO find first occurence  in the document
# x=mycollection.find_one()
# #To find all the entries in the collection
# for x in mycollection.find():
#     print(x)
#To Return Only Some Fields
# for x in mycollection.find({},{"_id":0,"name":1,"address":1}): #if specified 0 and 1 in name in address respectively it will return error
#     print(x)
#TO return or neglect a particular field
# for x in mycollection.find({},{"address":0}):
#     print(x)
#Similarly,for name also
#TO filter the result with a query
# query={"address":"Park Lane 38"}
# mydoc=mycollection.find(query)
# for x in mydoc:
#     print(x)
#Advanced Query # to find the documents where the "address" field starts with the letter "S" or higher (alphabetically),
# use the greater than modifier: {"$gt": "S"}:
# myquery={"address":{"$gt":"S"}}
# mydoc=mycollection.find(myquery)
# for x in mydoc:
#     print(x)
# To find only the documents where the "address" field starts with the letter "S"
query={"address":{"$regex":"^S"}}#$regex is remembered as regular expression
# mydoc=mycollection.find(query)
# for x in mydoc:
#     print(x)
#Sort the output
#For sort in both ascending use 1 as second parameter and for descending use -1
mydoc=mycollection.find().sort("name")
for x in mydoc:
    print(x)
mydesc=mycollection.find().sort("name",-1)
for x in mydesc:
    print(x)


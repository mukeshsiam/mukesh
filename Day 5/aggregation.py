import pymongo
from pymongo import MongoClient
myclient=MongoClient("mongodb://localhost:27017/")
mydb=myclient["mukidb1"]
mycollection=mydb["students"]
data = [
    {"user":"Krish", "subject":"Database", "score":80},
    {"user":"Amit",  "subject":"JavaScript", "score":90},
    {"user":"Amit",  "title":"Database", "score":85},
    {"user":"Krish",  "title":"JavaScript", "score":75},
    {"user":"Amit",  "title":"Data Science", "score":60},
    {"user":"Krish",  "title":"Data Science", "score":95}]
mycollection.insert_many(data)

### Find Amit And Krish Total Subjects
# agg_result= mycollection.aggregate(
#     [{
#     "$group" :
#         {"_id" : "$user",
#          "Total Subject" : {"$sum" : 1}
#          }}
#     ])
agg_result2=mycollection.aggregate(
    [
        {"$group":
            {"_id":"$user",
             "Total marks":{'$sum':"$score"}

        }
        }
    ]
)
for i in agg_result2:
    print(i)


import pymongo
# print(dir(pymongo))
# import pprint
# print(dir(pprint))
myclient = pymongo.MongoClient("mongodb://localhost:27017/")

mydb=myclient["mukidb1"]
mycollection=mydb["customers"]
#TO Insert One document to collection
mydict={"muki":"chennai","raj":"mumbai","sam":"goa"}
x=mycollection.insert_one(mydict)
print(x.inserted_id)
##TO Insert Many document to collection
# mylist=[{ "name": "Amy", "address": "Apple st 652"},
#   { "name": "Hannah", "address": "Mountain 21"},
#   { "name": "Michael", "address": "Valley 345"},
#   { "name": "Sandy", "address": "Ocean blvd 2"},
#   { "name": "Betty", "address": "Green Grass 1"},
#   { "name": "Richard", "address": "Sky st 331"},
#   { "name": "Susan", "address": "One way 98"},
#   { "name": "Vicky", "address": "Yellow Garden 2"},
#   { "name": "Ben", "address": "Park Lane 38"},
#   { "name": "William", "address": "Central st 954"},
#   { "name": "Chuck", "address": "Main Road 989"},
#   { "name": "Viola", "address": "Sideway 1633"}
# ]
# x=mycollection.insert_many(mylist)
# print(myclient.list_database_names())
# print(x.inserted_ids) #inserted_id & inserted_ids

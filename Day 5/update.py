import pymongo
url="mongodb://localhost:27017/"
myclient=pymongo.MongoClient(url)
mydb=myclient["mukidb1"]
mycollection=mydb["customers"]
#Change the address from "Valley 345" to "Canyon 123":
# query={"address":"Valley 345"}
# newvalue={"$set":{"address":"Canyon 123"}}
# update=mycollection.update_one(query,newvalue)
# for x in mycollection.find():
#     print(x)
#TO update many
# myquery = { "address": { "$regex": "^S" } }
# newvalues = { "$set": { "name": "Muki" } }
# x=mycollection.update_many(myquery,newvalues)
# print(x.modified_count)
# for x in mycollection.find():
#     print(x)
#To limit the output of the collecttion
myresult=mycollection.find().limit(7)
for x in myresult:
    print(x)
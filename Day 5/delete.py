import pymongo
url="mongodb://localhost:27017/"
myclient=pymongo.MongoClient(url)
mydb=myclient["mukidb1"]
mycollection=mydb["customers"]
#To delete one Document
# query={"name":"Betty"}
# d=mycollection.delete_one(query)
#To delete many
# myquery = { "address": {"$regex": "^S"} }
# remove=mycollection.delete_many(myquery)
# print(remove.deleted_count)
# mydoc=mycollection.find()
# for x in mydoc:
#     print(x)
#To delete a collection
# drop=mycollection.drop()
# print(drop)
# print(mydb.list_collection_names())
# for x in mycollection.find():
#     print(x)
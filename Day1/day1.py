#1.Print Hello World
print("Hello World")
#2.Defining global variable
x = "awesome"
def myfunc():
  global x
  x = "fantastic"
myfunc()
print("Python is " + x)
#Strings
a = " Hello, World! "
print(a.strip()) # returns "Hello, World!"
#format
age=21
txt="I am {}"
print(txt.format(age))
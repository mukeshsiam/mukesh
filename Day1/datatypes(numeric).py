# Numeric Datatypes in Phython
print(type(123))
print(type(123.3))
print(type(5+3j))
print(type(4>5))
#Numeric Datatypes(int,float,complex,boolean)
print(int(9.8))   #float converted to integer
print(float(9))     #int cov to float
a=8
b=4
c=complex(a,b)   #complex function used to convert to complex numbers
print(c)
#Boolean
print(a>b)
print(type(a<b))
print(int(True))  #true is 1
print(float(False)) #false is 0
